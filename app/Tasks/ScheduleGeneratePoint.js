'use strict'

const Task = use('Task')
const ArangoService = require('../Controllers/Http/Helper/ArangoService.js')

class ScheduleGeneratePoint extends Task {
  static get schedule () {
    return '0 */60 * * * *'
  }

  async handle () {
    console.log("Generating AINO Points for Users")
    ArangoService.generatePoint()
  }
}

module.exports = ScheduleGeneratePoint
