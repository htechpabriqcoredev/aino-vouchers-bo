'use strict'

const Task = use('Task')

class ScheduleIt extends Task {
	static get schedule () {
		return '30 * * * *'
	}

	async handle () {
		this.info('Task ScheduleIt handle '+new Date())
	}
}

module.exports = ScheduleIt
