'use strict'

const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')

class SettingController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Setting', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}

		let dataSetting = []

		for(let x in settings) {
			dataSetting[settings[x]['key']] = settings[x]['value']
		}
			
		let template = view.render('setting.index', {
			meta: meta,
			settings: dataSetting,
		})
		
		return await minifyHTML.minify(template)
	}
	
	async updateall({request, response, auth, view, session}) {
		const formData = request.all()

		let nameShortcutIcon
		let nameBigLogo
		let nameSmallLogo

		// shortcut_icon
		const shortcutIconPic = request.file('shortcut_icon', {
		    types: ['image'],
		    size: '10mb'
		})

		if(shortcutIconPic != null) {
			const type = request.file('shortcut_icon').subtype;
			nameShortcutIcon = `${new Date().getTime()}_shortcuticon.${type}`;
			
			await shortcutIconPic.move(Helpers.publicPath('/uploads/images/setting'), {
				name: nameShortcutIcon
			})

			if (!shortcutIconPic.moved()) {
			    return shortcutIconPic.error()
			}
		}

		// big_logo
		const bigLogoPic = request.file('big_logo', {
		    types: ['image'],
		    size: '10mb'
		})

		if(bigLogoPic != null) {
			const type = request.file('big_logo').subtype;
			nameBigLogo = `${new Date().getTime()}_biglogo.${type}`;
			
			await bigLogoPic.move(Helpers.publicPath('/uploads/images/biglogo'), {
				name: nameBigLogo
			})

			if (!bigLogoPic.moved()) {
			    return bigLogoPic.error()
			}
		}

		// small_logo
		const smallLogoPic = request.file('small_logo', {
		    types: ['image'],
		    size: '10mb'
		})

		if(smallLogoPic != null) {
			const type = request.file('small_logo').subtype;
			nameSmallLogo = `${new Date().getTime()}_smalllogo.${type}`;
			
			await smallLogoPic.move(Helpers.publicPath('/uploads/images/smalllogo'), {
				name: nameSmallLogo
			})

			if (!smallLogoPic.moved()) {
			    return smallLogoPic.error()
			}
		}
		
		await Setting.query().where('id', '1').update({ value: formData.meta_title })
		await Setting.query().where('id', '2').update({ value: formData.meta_description })
		await Setting.query().where('id', '3').update({ value: formData.meta_keyword })

		await Setting.query().where('id', '4').update({ value: formData.web_name })
		await Setting.query().where('id', '5').update({ value: formData.web_owner })
		await Setting.query().where('id', '6').update({ value: formData.address })
		await Setting.query().where('id', '7').update({ value: formData.email })
		await Setting.query().where('id', '8').update({ value: formData.telephone })
		await Setting.query().where('id', '9').update({ value: formData.fax })

		if(shortcutIconPic != null) {
			await Setting.query().where('id', '10').update({ value: '/uploads/images/setting/' + nameShortcutIcon })
		}

		if(bigLogoPic != null) {
			await Setting.query().where('id', '11').update({ value: '/uploads/images/biglogo/' + nameBigLogo })
		}

		if(smallLogoPic != null) {
			await Setting.query().where('id', '12').update({ value: '/uploads/images/smalllogo/' + nameSmallLogo })
		}

		session.flash({ notification: 'Setting updated', status: 'success' })
		return response.redirect('/setting')
	}
}

module.exports = SettingController