'use strict'

const Reward = use('App/Models/Reward')
const Loyalty = use('App/Models/Loyalty')
const LoyaltyDetails = use('App/Models/LoyaltyDetails')
const PrincipalConversion = use('App/Models/PrincipalConversion')
const Principal = use('App/Models/Principal')
const Merchant = use('App/Models/Merchant')
const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment')
const Helpers = use('Helpers')

class VoucherManagementController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('vouchermanagement.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		let tableDefinition
		
		if (auth.user.id == 1) {
			tableDefinition = {
				sTableName: 'rewards',
				sSelectSql: ['rewards.id', 'loyalty_details.category_name', 'loyalty_details.brand', 'rewards.name', 'loyalty_details.expiry_date', 'loyalty_details.price', 'rewards.point_required', 'rewards.term_condition', 'loyalty_details.detail', 'loyalty_details.location', 'loyalty_details.quota', 'rewards.status', 'rewards.quantity', 'rewards.location'],
				aSearchColumns: ['loyalty_details.category_name', 'loyalty_details.brand', 'rewards.name'],
				sFromSql: "rewards INNER JOIN loyalty_details ON rewards.loyalty_detail_id=loyalty_details.id INNER JOIN loyalties ON loyalty_details.loyalty_id= loyalties.id",
				dbType: 'postgres',
			}
		} else {
			tableDefinition = {
				sTableName: 'rewards',
				sSelectSql: ['rewards.id', 'loyalty_details.category_name', 'loyalty_details.brand', 'rewards.name', 'loyalty_details.expiry_date', 'loyalty_details.price', 'rewards.point_required', 'rewards.term_condition', 'loyalty_details.detail', 'loyalty_details.location', 'loyalty_details.quota', 'rewards.status', 'rewards.quantity', 'rewards.location'],
				aSearchColumns: ['loyalty_details.category_name', 'loyalty_details.brand', 'rewards.name'],
				sFromSql: "rewards INNER JOIN loyalty_details ON rewards.loyalty_detail_id=loyalty_details.id INNER JOIN loyalties ON loyalty_details.loyalty_id= loyalties.id INNER JOIN users on users.principal_id = loyalties.principal_id",
				sWhereAndSql: 'users.id ='+auth.user.id,
				dbType: 'postgres',
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		let new_expiry_date
		for(let x in selectRow) {
			if(selectRow[x]['expiry_date'] == "" || selectRow[x]['expiry_date'] == null){
				new_expiry_date = ""
			}else{
				new_expiry_date = moment(selectRow[x]['expiry_date'],'MM/DD/YYYY').format('MM/DD/YYYY')
			}
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['category_name'],
				selectRow[x]['brand'],
				selectRow[x]['name'],
				new_expiry_date,
				'Rp '+ parseInt(selectRow[x]['price']).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
				selectRow[x]['point_required'],
				selectRow[x]['quota'] == '' || selectRow[x]['quota'] == null ? 'Unlimited' : selectRow[x]['quota'] ,
				selectRow[x]['quantity'] == '' || selectRow[x]['quantity'] == null ? 'Unlimited' : selectRow[x]['quantity'] ,
				selectRow[x]['status'] == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Non Active</span>",
				"<div class='text-center'>\
					<a href='./vouchermanagement/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		let category
		if( auth.user.id == 1) {
			category = await Database.table('loyalty_details').distinct('loyalty_details.category_name')
							.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
							.leftJoin('users', 'users.principal_id', 'loyalties.principal_id')
		} else {
			category = await Database.table('loyalty_details').distinct('loyalty_details.category_name')
							.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
							.leftJoin('users', 'users.principal_id', 'loyalties.principal_id')
							.where('users.id', auth.user.id)
		}
		
		let template = view.render('vouchermanagement.create', {
			meta: meta,
			category: category
		})
		
		return await minifyHTML.minify(template)
	}

	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { category_id, category_name, loyalty_detail_id, reward_name, term_condition, location, quantity, point_required, reward_picture_default, quota } = request.only(['category_id', 'category_name', 'loyalty_detail_id', 'reward_name', 'term_condition', 'location', 'quantity', 'point_required', 'reward_picture_default', 'quota'])
				
		let nameReward
		const rewardPic = request.file('reward_picture', {
		    types: ['image'],
		    size: '10mb'
		})

		if(rewardPic != null) {
			const type = request.file('reward_picture').subtype;
			nameReward = `${new Date().getTime()}_rewardpicture_${auth.user.id}.${type}`;
			
			await rewardPic.move(Helpers.publicPath('/uploads/images/rewardpicture'), {
				name: nameReward
			})

			if (!rewardPic.moved()) {
			    return rewardPic.error()
			}
		}

		if(rewardPic == null && rewardPic == null) {
			nameReward = reward_picture_default;
		} else {
			nameReward = Env.get('BASE_URL') + '/uploads/images/rewardpicture/' + nameReward;
		}

		let formData = {
			loyalty_detail_id: loyalty_detail_id, 
			name: reward_name, 
			term_condition: term_condition, 
			location: location,  
			point_required: point_required, 
			status: 1,
			images: nameReward,
			category_id: category_id,
			category_name: category_name,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}

		if(quantity == "unlimited" || quantity == "Unlimited"){
			formData.quantity = null
		} else {
			formData.quantity = quantity
		}
		
		const rules = {
			name: 'required',
			point_required: 'required',
			term_condition: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Reward.create(formData)
			session.flash({ notification: 'Reward added', status: 'success' })
			return response.redirect('/vouchermanagement')
		}
	}

	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		let reward = await Reward.find(params.id)
		if (reward) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}

			let loyalty_detail = await LoyaltyDetails.query().where('id', reward.loyalty_detail_id).first()
			
			let template = view.render('vouchermanagement.edit', {
				meta: meta,
				reward: reward,
				loyalty_detail: loyalty_detail.toJSON(),
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Reward not found', status: 'danger' })
			return response.redirect('/vouchermanagement')
		}
	}

	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { status, term_condition, quantity, point_required, price, quota, reward_picture_default, loyalty_detail_id} = request.only(['status', 'term_condition', 'quantity', 'point_required', 'price', 'quota', 'reward_picture_default', 'loyalty_detail_id'])
		
		let voucher = await Reward.find(params.id)

		let nameReward
		const rewardPic = request.file('reward_picture', {
		    types: ['image'],
		    size: '10mb'
		})

		if(rewardPic != null) {
			const type = request.file('reward_picture').subtype;
			nameReward = `${new Date().getTime()}_rewardpicture_${auth.user.id}.${type}`;
			
			await rewardPic.move(Helpers.publicPath('/uploads/images/rewardpicture'), {
				name: nameReward
			})

			if (!rewardPic.moved()) {
			    return rewardPic.error()
			}
		}

		if(rewardPic == null && rewardPic == null) {
			nameReward = reward_picture_default;
		} else {
			nameReward = Env.get('BASE_URL') + '/uploads/images/rewardpicture/' + nameReward;
		}

		let formData = {
			term_condition: term_condition,
			point_required: point_required,
			//price: price,
			//quota: quota,
			status: status,
			images: nameReward,
			updated_by: auth.user.id
		}

		if(quantity == "unlimited" || quantity == "Unlimited"){
			formData.quantity = null
		} else {
			formData.quantity = quantity
		}
		
		let rules = {
			term_condition: 'required',
			point_required: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (voucher) {
				await Reward.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Reward updated', status: 'success' })
				return response.redirect('/vouchermanagement')
			} else {
				session.flash({ notification: 'Reward not found', status: 'danger' })
				return response.redirect('/vouchermanagement')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let reward = await Reward.find(formData.id)
		if (reward){
			try {
				await reward.delete()
				session.flash({ notification: 'Voucher success deleted', status: 'success' })
				return response.redirect('/vouchermanagement')
			} catch (e) {
				session.flash({ notification: 'Voucher cannot be delete', status: 'danger' })
				return response.redirect('/vouchermanagement')
			}
		} else {
			session.flash({ notification: 'Voucher cannot be delete', status: 'danger' })
			return response.redirect('/vouchermanagement')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('VoucherManagement', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let reward = await Reward.find(formData.item[i])
				try {
					await reward.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Voucher success deleted', status: 'success' })
			return response.redirect('/vouchermanagement')
		} else {
			session.flash({ notification: 'Voucher cannot be deleted', status: 'danger' })
			return response.redirect('/vouchermanagement')
		}
	}

	async getDataVoucher({ request, response, auth, view, session }) {
		let req = request.all()
		let data

		if (auth.user.user_role == '1' || auth.user.user_role == '2') {
			data = await Reward.query()
				.select('rewards.id', 'rewards.name')
				.where('rewards.status', 1)
				.orderBy('rewards.id', 'asc')
				.fetch()
		} else {
			data = await Reward.query()
				.select('rewards.id', 'rewards.name')
				.leftJoin('loyalty_details', 'loyalty_details.id', 'rewards.loyalty_detail_id')
				.leftJoin('loyalties', 'loyalties.id', 'loyalty_details.loyalty_id')
				.where('loyalties.principal_id', auth.user.principal_id)
				.where('rewards.status', 1)
				.orderBy('rewards.id', 'asc')
				.fetch()
		}

		let dataJson = data.toJSON()

		if(data) {
			data = {
				code: '2000',
				message: 'Reward Found',
				data: dataJson
			}
		} else {
			data = {
				code: '4000',
				message: 'Reward Not Found',
				data: []
			}
		}
		return response.send(data)
	}

	async getVoucher({ request, response, auth, view, session }) {
		let req = request.all()
		let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
		let principals = principal.toJSON()
		let reward = await Reward.query().select('loyalty_detail_id').fetch()
		let rewardval = reward.toJSON()
		let loyaltyDetails
		if (principals.principal_id == null || principals.principal_id == ''){
			loyaltyDetails = await LoyaltyDetails.query()
						.select('loyalty_details.id', 'loyalty_details.name', 'loyalty_details.loyalty_id', 'loyalty_details.category_name', 'loyalty_details.brand', 'loyalty_details.expiry_date', 'loyalty_details.price', 'loyalty_details.detail', 'loyalty_details.location', 'loyalty_details.quota')
						.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
						.where('loyalty_details.category_name', req.category_id)
						.andWhere('loyalty_details.brand', req.brand)
		} else {
			loyaltyDetails = await LoyaltyDetails.query()
						.select('loyalty_details.id', 'loyalty_details.name', 'loyalty_details.loyalty_id', 'loyalty_details.category_name', 'loyalty_details.brand', 'loyalty_details.expiry_date', 'loyalty_details.price', 'loyalty_details.detail', 'loyalty_details.location', 'loyalty_details.quota')
						.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
						.where('loyalties.principal_id', principals.principal_id)
						.where('loyalty_details.category_name', req.category_id)
						.andWhere('loyalty_details.brand', req.brand)
						//.whereNot('loyalty_details.id', '=', reward.loyalty_detail_id)
		}

		return response.send(loyaltyDetails)
	}

	async getDetail({ request, response, auth, view, session }) {
		let req = request.all()

		let loyaltyDetails = await LoyaltyDetails.query().where('id', req.loyalty_detail_id)
		
		let chck = await Reward.query().where('loyalty_detail_id', req.loyalty_detail_id)

		if(chck == '' || chck == null){
			return response.send(loyaltyDetails)
		} else {
			return 0
		}
	}

	async getBrand({ request, response, auth, view, session }) {
		let req = request.get()
		let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
		let principals = principal.toJSON()
		
		let brand 
		if(principals.principal_id == null || principals.principal_id == ''){
			brand = await LoyaltyDetails.query()
				.select('brand')
				.where('brand', 'LIKE', '%' + req.q + '%')
				.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
				.where('category_name', req.category)
				.distinct('brand')
				.fetch()
		} else {
			brand = await LoyaltyDetails.query()
				.select('brand')
				.where('brand', 'LIKE', '%' + req.q + '%')
				.leftJoin('loyalties', 'loyalty_details.loyalty_id', 'loyalties.id')
				.where('loyalties.principal_id', principals.principal_id)
				.where('category_name', req.category)
				.distinct('brand')
				.fetch()
		}
		

		let brands = brand.toJSON()
		
		let data = []
		for(let i in brands){
			data.push({
				id: brands[i]['brand'],
				text: brands[i]['brand']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async convertPoint({ request, response, auth, view, session }) {
		let req = request.all()

		let getPointPrincipal = await PrincipalConversion.query().where('user_id', req.user_id).first()
		let conversion = getPointPrincipal.amount
		let price = req.price

		let result_point = Math.floor(price/conversion)

		return response.send(result_point)
	}
}

module.exports = VoucherManagementController