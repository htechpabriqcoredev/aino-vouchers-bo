'use strict'

const SystemLog = use('App/Models/SystemLog')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class SystemLogController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('systemlog.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'system_logs',
			sSelectSql: ['id', 'user_id', 'access', 'ip', 'user_agent', 'browser', 'cpu', 'device', 'engine', 'os', 'url', 'method', 'param', 'body', 'response'],
			aSearchColumns: ['user_id', 'access', 'ip', 'user_agent', 'browser', 'cpu', 'device', 'engine', 'os', 'url', 'method', 'param', 'body', 'response']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['user_id'],
				select[0][x]['access'],
				select[0][x]['ip'],
				select[0][x]['user_agent'],
				select[0][x]['browser'],
				select[0][x]['cpu'],
				select[0][x]['device'],
				select[0][x]['engine'],
				select[0][x]['os'],
				select[0][x]['url'],
				select[0][x]['method'],
				select[0][x]['param'],
				select[0][x]['body'],
				select[0][x]['response'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('systemlog.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { user_id, access, ip, user_agent, browser, cpu, device, engine, os, url, method, param, body, response } = request.only(['user_id', 'access', 'ip', 'user_agent', 'browser', 'cpu', 'device', 'engine', 'os', 'url', 'method', 'param', 'body', 'response'])
		
		let formData = {
			user_id: user_id,
			access: access,
			ip: ip,
			user_agent: user_agent,
			browser: browser,
			cpu: cpu,
			device: device,
			engine: engine,
			os: os,
			url: url,
			method: method,
			param: param,
			body: body,
			response: response,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			user_id: 'required',
			access: 'required',
			ip: 'required',
			user_agent: 'required',
			browser: 'required',
			cpu: 'required',
			device: 'required',
			engine: 'required',
			os: 'required',
			url: 'required',
			method: 'required',
			param: 'required',
			body: 'required',
			response: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await SystemLog.create(formData)
			session.flash({ notification: 'System log added', status: 'aquamarine' })
			return response.redirect('/systemlog')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let systemlog = await SystemLog.find(params.id)

		if (systemlog) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('systemlog.edit', {
				meta: meta,
				systemlog: systemlog
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'System log not found', status: 'danger' })
			return response.redirect('/systemlog')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { user_id, access, ip, user_agent, browser, cpu, device, engine, os, url, method, param, body, response } = request.only(['user_id', 'access', 'ip', 'user_agent', 'browser', 'cpu', 'device', 'engine', 'os', 'url', 'method', 'param', 'body', 'response'])
		
		let systemlog = await SystemLog.find(params.id)
		
		let formData = {
			user_id: user_id,
			access: access,
			ip: ip,
			user_agent: user_agent,
			browser: browser,
			cpu: cpu,
			device: device,
			engine: engine,
			os: os,
			url: url,
			method: method,
			param: param,
			body: body,
			response: response,
			
			updated_by: auth.user.id
		}
		
		let rules = {
			user_id: 'required',
			access: 'required',
			ip: 'required',
			user_agent: 'required',
			browser: 'required',
			cpu: 'required',
			device: 'required',
			engine: 'required',
			os: 'required',
			url: 'required',
			method: 'required',
			param: 'required',
			body: 'required',
			response: 'required',
			
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (systemlog) {
				await SystemLog.query().where('id', params.id).update(formData)
				session.flash({ notification: 'System log updated', status: 'aquamarine' })
				return response.redirect('/systemlog')
			} else {
				session.flash({ notification: 'SystemLog not found', status: 'danger' })
				return response.redirect('/systemlog')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		let systemlog = await SystemLog.find(formData.id)
		if (systemlog){
			try {
				await systemlog.delete()
				session.flash({ notification: 'System log success deleted', status: 'aquamarine' })
				return response.redirect('/systemlog')
			} catch (e) {
				session.flash({ notification: 'System log cannot be delete', status: 'danger' })
				return response.redirect('/systemlog')
			}
		} else {
			session.flash({ notification: 'System log cannot be delete', status: 'danger' })
			return response.redirect('/systemlog')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('SystemLog', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let systemlog = await SystemLog.find(formData.item[i])
				try {
					await systemlog.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'System log success deleted', status: 'aquamarine' })
			return response.redirect('/systemlog')
		} else {
			session.flash({ notification: 'System log cannot be deleted', status: 'danger' })
			return response.redirect('/systemlog')
		}
	}
}

module.exports = SystemLogController