'use strict'
const Validation = require('joi')

class MasterService {
	async getPrincipals(){
		let result = {
			success : false,
			data : {},
			message : ""
		}

		const data = {
			"count": 39,
			"data": [
				{
					"mid": "10121",
					"name": "BENCHMARKSUBGROUP2.1",
					"extras": null,
					"logo": "",
					"additional_documents": null
				},
				{
					"mid": "10267246",
					"name": "warung pintar",
					"extras": {
						"npwp": "1",
						"address": "asd, Актюбинская область, Kazakhstan",
						"npwp_name": "pak abc",
						"disbursement": null,
						"director_name": "pak abc",
						"contract_number": "345678",
						"contract_end_date": "2019-04-30 14:38:31",
						"contract_start_date": "2019-04-01 00:00:00",
						"director_phone_number": "1234"
					},
					"logo": "",
					"additional_documents": null
				},
				{
					"mid": "0102030405",
					"name": "Coba Mapping MID",
					"extras": {
						"npwp": null,
						"address": null,
						"npwp_name": null,
						"pos_payment": null,
						"qr_code_edc": null,
						"account_name": null,
						"bank_account": null,
						"disbursement": null,
						"director_name": null,
						"account_number": null,
						"director_email": null,
						"contract_number": null,
						"qr_code_payment": null,
						"website_payment": null,
						"pos_payment_next": null,
						"contract_end_date": null,
						"settlement_periode": null,
						"contract_start_date": null,
						"number_of_employees": null,
						"expected_transaction": null,
						"director_phone_number": null,
						"director_detail_address": null,
						"average_payment_delivery": null,
						"director_identity_number": null,
						"maximum_payment_delivery": null,
						"average_amount_transaction": null,
						"maximum_amount_transaction": null
					},
					"logo": "",
					"additional_documents": []
				},
				{
					"mid": "6969696969",
					"name": "Gembira Loka Zoo",
					"extras": {
						"npwp": "69696969696968",
						"address": "Jalan Acasia, Catur Tunggal, Daerah Istimewa Yogyakarta, Indonesia",
						"npwp_name": "NPWP GLZoo",
						"pos_payment": null,
						"qr_code_edc": null,
						"account_name": null,
						"bank_account": null,
						"disbursement": null,
						"director_name": "Folder GLZoo",
						"account_number": null,
						"director_email": "Email GLZoo",
						"contract_number": "69",
						"qr_code_payment": null,
						"website_payment": null,
						"pos_payment_next": null,
						"contract_end_date": "2069-11-02 17:43:10",
						"settlement_periode": null,
						"contract_start_date": "2019-05-02 17:43:10",
						"number_of_employees": null,
						"expected_transaction": null,
						"director_phone_number": "+62 85 696969696",
						"director_detail_address": "PT Aino Indonesia",
						"average_payment_delivery": null,
						"director_identity_number": "1",
						"maximum_payment_delivery": null,
						"average_amount_transaction": null,
						"maximum_amount_transaction": null
					},
					"logo": "",
					"additional_documents": []
				},
				{
					"mid": "20001",
					"name": "SatationA",
					"extras": null,
					"logo": "",
					"additional_documents": null
				},
				{
					"mid": "23273251",
					"name": "TOLL",
					"extras": {
						"npwp": "503248750320543",
						"address": "Jalan Raya Tajem, Maguwoharjo, Daerah Istimewa Yogyakarta, Indonesia",
						"npwp_name": "Firmansyah",
						"disbursement": null,
						"director_name": "Firmansyah",
						"contract_number": "980724987532442",
						"contract_end_date": "2025-12-31 15:36:25",
						"contract_start_date": "2019-03-25 15:36:25",
						"director_phone_number": "08123456789"
					},
					"logo": "",
					"additional_documents": null
				},
				{
					"mid": "23309692",
					"name": "Imantoko Pokoknya Jos",
					"extras": {
						"npwp": "9011808881118280",
						"address": "Braşov, Brașov, Romania",
						"npwp_name": "Imantoko",
						"disbursement": null,
						"director_name": "Imantoko",
						"contract_number": "9828211172717",
						"contract_end_date": "2019-04-12 14:57:32",
						"contract_start_date": "2019-04-05 14:57:32",
						"director_phone_number": "0852321112412"
					},
					"logo": "uploads/merchants/8495e9096d08318a4354e121e547be07.jpg",
					"additional_documents": null
				},
				{
					"mid": "99897029",
					"name": "Toko Minion",
					"extras": {
						"npwp": "45678909876",
						"address": "Surakarta, Jawa Tengah, Indonesia",
						"npwp_name": "zaki",
						"pos_payment": null,
						"qr_code_edc": null,
						"account_name": null,
						"bank_account": null,
						"disbursement": null,
						"director_name": "Zaki",
						"account_number": null,
						"director_email": "zaki@minion.com",
						"contract_number": "663",
						"qr_code_payment": null,
						"website_payment": null,
						"pos_payment_next": null,
						"contract_end_date": "2019-08-03 09:55:43",
						"settlement_periode": null,
						"contract_start_date": "2019-05-04 09:55:43",
						"number_of_employees": null,
						"expected_transaction": null,
						"director_phone_number": "081345678",
						"director_detail_address": "jalan minon",
						"average_payment_delivery": null,
						"director_identity_number": "3456789",
						"maximum_payment_delivery": null,
						"average_amount_transaction": null,
						"maximum_amount_transaction": null
					},
					"logo": "uploads/merchants/a8dfdfca0accb07e09c565dae33a68b9.jpg",
					"additional_documents": []
				},
				{
					"mid": "31578631",
					"name": "MRT Jakarta Testing",
					"extras": {
						"npwp": "1",
						"address": "Jalan Parangkusumo, Daerah Istimewa Yogyakarta, Indonesia",
						"npwp_name": "Lustria Risyal",
						"director_name": "Lustria Risyal",
						"contract_number": "1",
						"contract_end_date": "2019-06-01 10:16:23",
						"contract_start_date": "2019-04-01 10:16:23",
						"director_phone_number": "08511223344"
					},
					"logo": "",
					"additional_documents": null
				},
				{
					"mid": "10021",
					"name": "SUBGROUPBENCHMARK2",
					"extras": null,
					"logo": "",
					"additional_documents": null
				}
			],
			"message": "ok",
			"status": true
		}
		
		result.success = true
		result.data = data

		return result
	}

	async getMerchants(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}

		const data = {
			"count": 75,
			"data": [
				{
					"mid": "90452734",
					"name": "Test Ebis Rabu",
					"extras": {
						"npwp": "34543",
						"allow": "1",
						"emoney": [
							"1",
							"4"
						],
						"address": "Sleman, Daerah Istimewa Yogyakarta, Indonesia",
						"products": [
							"1"
						],
						"sharefee": "[{\"name\":\"key ebis rabu\"}]",
						"npwp_name": "fdsfdsf",
						"director_name": "Ebis",
						"contract_number": "435345",
						"contract_end_date": "2019-03-26 09:54:34",
						"contract_start_date": "2019-03-07 09:54:34",
						"director_phone_number": "12232334"
					},
					"logo": "",
					"additional_documents": null
				}
			],
			"message": "ok",
			"status": true
		}

		const schema = Validation.object().keys({
			mid : Validation.string().min(1).max(50).required(),
		})
		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				result.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})
		if (validationprc){
			try{
				result.success = true
				result.data = data
			} catch(e) {
				result.message = e.message
			}
		}

		return result
	}

	async getIssuers(){
		let result = {
			success : false,
			data : {},
			message : ""
		}

		const data = {
			"count": 2,
			"data": [
				{
					"issuer_name": "DKI",
					"issuer_emoney_id": 7,
					"issuer_emoney_name": "JAKCARD2",
					"issuer_emoney_code": "JAKCARD2",
					"issuer_emoney_type": "",
					"issuer_extras": null
				},
				{
					"issuer_name": "DKI",
					"issuer_emoney_id": 5,
					"issuer_emoney_name": "JAKCARD",
					"issuer_emoney_code": "JAKCARD",
					"issuer_emoney_type": "",
					"issuer_extras": null
				}
			],
			"message": "ok",
			"status": true
		}

		result.success = true
		result.data = data

		return result
	}
}

module.exports = new MasterService