'use strict'
const Validation = require('joi')
const arangojs = require("arangojs")
const Card = use('App/Models/Card')
const Rule = use('App/Models/Rule')
const UserPoint = use('App/Models/UserPoint')
const Setting = use('App/Models/Setting')
const TransactionEarn = use('App/Models/TransactionEarn')
const PointNotification = use('App/Models/PointNotification')
const Env = use('Env')
//ssh -R ainoarrango.serveo.net:8529:localhost:8529 serveo.net

let db = false
let dberr
try{
	db = new arangojs.Database({ url: Env.get('ARANGO_HOST') })
} catch (e) { 
	db = false
	dberr = e.message 
}

class ArangoService {
	async getScdAll(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}
		const schema = Validation.object().keys({
			page :  Validation.number().integer().optional(),
			perPage :  Validation.number().integer().optional()
		})
		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				result.message  = err.message[0]
			} else {
				if(value){
					validation = value
				} else{
					validation = true
				}
			}
			return validation
		})
		if (validationprc){
			try{
				if(db){
					let q = 'FOR d IN multi_channel_activity SORT d.transDtTm ASC RETURN d'
					if(validationprc.page && validationprc.perPage){
						const skip = (validationprc.page - 1) * validationprc.perPage
						q = 'FOR d IN multi_channel_activity SORT d.transDtTm ASC LIMIT '+skip+', '+validationprc.perPage+' RETURN d'
					}
					db.useDatabase(Env.get('ARANGO_DATABASE'))
					db.useBasicAuth(Env.get('ARANGO_USER'), Env.get('ARANGO_PASSWORD'))
					const query = await db.query(q)
					const data = await query.all()
					result.success = true
					result.data = data
				} else {
					result.message = dberr
				}
			} catch(e) {
				result.message = e.message
			}
		}
		return result
	}

	async getScdByPrincipalId(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}
		const schema = Validation.object().keys({
			principalId : Validation.string().min(1).max(50).required(),
			page :  Validation.number().integer().optional(),
			perPage :  Validation.number().integer().optional()
		})
		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				result.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})
		if (validationprc){
			try{
				if(db){
					let q = 'FOR d IN multi_channel_activity FILTER CONTAINS(LEFT(d.mid, 4), "'+validationprc.principalId+'") SORT d.transDtTm ASC RETURN d'
					if(validationprc.page && validationprc.perPage){
						const skip = (validationprc.page - 1) * validationprc.perPage
						q = 'FOR d IN multi_channel_activity FILTER CONTAINS(LEFT(d.mid, 4), "'+validationprc.principalId+'") SORT d.transDtTm ASC LIMIT '+skip+', '+validationprc.perPage+' RETURN d'
					}
					db.useDatabase(Env.get('ARANGO_DATABASE'))
					db.useBasicAuth(Env.get('ARANGO_USER'), Env.get('ARANGO_PASSWORD'))
					const query = await db.query(q)
					const data = await query.all()
					result.success = true
					result.data = data
				} else {
					result.message = dberr
				}
			} catch(e) {
				result.message = e.message
			}
		}
		return result
	}

	async generatePoint(){
		let result = {
			success : false,
			data : {},
			message : ""
		}
		let last_data = "1945-07-17 00:00:00.000"
		try{
			const setting = await Setting.query()
			.where('code', 'server')
			.where('key', 'scd_last_check')
			.first()
			if(setting) {
				last_data = setting.value
			} else {
				await Setting.create(
					{
						code : 'server',
						key : 'scd_last_check',
						value : last_data
					}
				)
			}
			if(db){
				db.useDatabase(Env.get('ARANGO_DATABASE'))
				db.useBasicAuth(Env.get('ARANGO_USER'), Env.get('ARANGO_PASSWORD'))
				const query = await db.query('FOR d IN multi_channel_activity FILTER d.transDtTm > "'+last_data+'" SORT d.transDtTm ASC RETURN d')
				const data = await query.all()
				if(data){
					for (const key in data) {
						last_data = data[key].transDtTm
						if(data[key].mid && data[key].issAcountId){
							let mid = data[key].mid
							mid = mid.substr(0, 4)
							const card = await Card.query()
							.where('card_number', data[key].issAcountId)
							.where('principal_id', mid)
							.first()
							if(card) {
								const rules = await Rule.query()
								//.where('issuer_id', card.issuer_id)
								.where('principal_id', card.principal_id)
								.where('period_start','<=',data[key].transDtTm)
								.where('period_end','>=',data[key].transDtTm)
								.where('status', 1)
								.fetch()
								if(rules){
									let data_i = rules.toJSON()
									for (const key_i in data_i) {
										let issuers_str = data_i[key_i].issuer_id
										issuers_str = issuers_str.replace(/"|{|}/gi, '')
										const issuers = issuers_str.split(",")
										const is_issuer = issuers.find(element => element == data[key].issId)
										if(is_issuer){
											let  point = 0
											const rwd_point = Math.floor(data[key].payAmt / data_i[key_i].spending_amount)
											point = Math.floor(rwd_point * data_i[key_i].reward_amount)
											if(point > 0){
												const userpoint = await UserPoint
													.query()
													.where('user_id', card.user_id)
													.where('principal_id', card.principal_id)
													.first()
												if (userpoint){
													point = userpoint.point + point
													await UserPoint
													.query()
													.where('id', userpoint.id)
													.update({ point: point})
												} else {
													const userPointData = {
														user_id : card.user_id,
														point : point,
														created_by : 1,
														updated_by : 1,
														principal_id : card.principal_id
													}
													await UserPoint.create(userPointData)
												}
												const transEarn = {
													user_id : card.user_id,
													issuer_id : data[key].issId,
													card_id : card.id,
													card_number : card.card_number,
													activity_type : data_i[key_i].activity_type,
													spending_type : data_i[key_i].spending_type,
													amount : data[key].payAmt,
													reward_amount : rwd_point,
													location : data[key].add.location,
													date : data[key].transDtTm,
													created_by : 1,
													updated_by : 1
												}
												await TransactionEarn.create(transEarn)
												const pointNotification = {
													user_id : card.user_id,
													principal_id : card.principal_id,
													title : "You get points",
													message : "you get " + rwd_point + " points",
													type : "POINT_001",
													created_by : 1,
													updated_by : 1
												}
												await PointNotification.create(pointNotification)
											}
										}
									}
								}
							}
						}
					}
				}
				result.success = true
				result.data = data
			} else {
				result.message = dberr
			}
			await Setting
			.query()
			.where('code', 'server')
			.where('key', 'scd_last_check')
			.update({ value: last_data})
		} catch(e) {
			result.message = e.message
		}
		return result
	}

	async generatePointByAcountId(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}

		const schema = Validation.object().keys({
			principalId : Validation.string().min(1).max(50).required(),
			cardNumber : Validation.string().min(1).max(50).required(),
			issuerId : Validation.string().min(1).max(50).required()
		})

		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				result.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})

		if (validationprc){
			let last_data
			try{
				const firstrule = await Rule.query()
				.where('principal_id', validationprc.principalId)
				.where('status', 1)
				.first()
				if(firstrule){
					let issuers_str = firstrule.issuer_id
					issuers_str = issuers_str.replace(/"|{|}/gi, '')
					const issuers = issuers_str.split(",")
					let is_issuer = issuers.find(element => element == validationprc.issuerId)
					if(is_issuer){
						const setting = await Setting.query()
						.where('code', 'server')
						.where('key', 'scd_last_check')
						.first()
						if(setting) {
							last_data = setting.value
						} 
						if(db){
							let q = 'FOR d IN multi_channel_activity FILTER CONTAINS(LEFT(d.mid, 4), "'+ validationprc.principalId
							+'") && d.issAcountId == "'+ validationprc.cardNumber
							+'" && d.transDtTm > "'+ firstrule.period_start
							+'" SORT d.transDtTm ASC RETURN d'
							if(last_data) {
								q = 'FOR d IN multi_channel_activity FILTER CONTAINS(LEFT(d.mid, 4), "'+ validationprc.principalId
								+'") && d.issAcountId == "'+ validationprc.cardNumber
								+'" && d.transDtTm < "'+ last_data
								+'" && d.transDtTm > "'+ firstrule.period_start
								+'" SORT d.transDtTm ASC RETURN d'
							} 
							db.useDatabase(Env.get('ARANGO_DATABASE'))
							db.useBasicAuth(Env.get('ARANGO_USER'), Env.get('ARANGO_PASSWORD'))
							const query = await db.query(q)
							const data = await query.all()
							if(data){
								for (const key in data) {
									last_data = data[key].transDtTm
									if(data[key].mid && data[key].issAcountId){
										let mid = data[key].mid
										mid = mid.substr(0, 4)
										const card = await Card.query()
										.where('card_number', data[key].issAcountId)
										.where('principal_id', mid)
										.first()
										if(card) {
											const rules = await Rule.query()
											//.where('issuer_id', card.issuer_id)
											.where('principal_id', card.principal_id)
											.where('period_start','<=',data[key].transDtTm)
											.where('period_end','>=',data[key].transDtTm)
											.where('status', 1)
											.fetch()
											if(rules){
												let data_i = rules.toJSON()
												for (const key_i in data_i) {
													let issuers_str = data_i[key_i].issuer_id
													issuers_str = issuers_str.replace(/"|{|}/gi, '')
													const issuers = issuers_str.split(",")
													const is_issuer = issuers.find(element => element == data[key].issId)
													if(is_issuer){
														let  point = 0
														const rwd_point = Math.floor(data[key].payAmt / data_i[key_i].spending_amount)
														point = Math.floor(rwd_point * data_i[key_i].reward_amount)
														if(point > 0){
															const userpoint = await UserPoint
																.query()
																.where('user_id', card.user_id)
																.where('principal_id', card.principal_id)
																.first()
															if (userpoint){
																point = userpoint.point + point
																await UserPoint
																.query()
																.where('id', userpoint.id)
																.update({ point: point})
															} else {
																const userPointData = {
																	user_id : card.user_id,
																	point : point,
																	created_by : 1,
																	updated_by : 1,
																	principal_id : card.principal_id
																}
																await UserPoint.create(userPointData)
															}
															const transEarn = {
																user_id : card.user_id,
																issuer_id : data[key].issId,
																card_id : card.id,
																card_number : card.card_number,
																activity_type : data_i[key_i].activity_type,
																spending_type : data_i[key_i].spending_type,
																amount : data[key].payAmt,
																reward_amount : rwd_point,
																location : data[key].add.location,
																date : data[key].transDtTm,
																created_by : 1,
																updated_by : 1
															}
															await TransactionEarn.create(transEarn)
															const pointNotification = {
																user_id : card.user_id,
																principal_id : card.principal_id,
																title : "You get points",
																message : "you get " + rwd_point + " points",
																type : "POINT_001",
																created_by : 1,
																updated_by : 1
															}
															await PointNotification.create(pointNotification)
														}
													}
												}
											}
										}
									}
								}
							}
							result.success = true
							result.data = data
						} else {
							result.message = dberr
						}
					}
				} 
			} catch(e) {
				result.message = e.message
			}
		}
		return result
	}

	async checkCardByAcountId(customs){
		let result = {
			success : false,
			connect : true,
			data : [],
			message : ""
		}

		const schema = Validation.object().keys({
			principalId : Validation.string().min(1).max(50).required(),
			cardNumber : Validation.string().min(1).max(50).required(),
			issuerId : Validation.string().min(1).max(50).required()
		})

		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				result.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})

		if (validationprc){
			let last_data
			try{
				const firstrule = await Rule.query()
				.where('principal_id', validationprc.principalId)
				.where('status', 1)
				.first()
				if(firstrule){
					let issuers_str = firstrule.issuer_id
					issuers_str = issuers_str.replace(/"|{|}/gi, '')
					const issuers = issuers_str.split(",")
					let is_issuer = issuers.find(element => element == validationprc.issuerId)
					if(is_issuer){
						const setting = await Setting.query()
						.where('code', 'server')
						.where('key', 'scd_last_check')
						.first()
						if(setting) {
							last_data = setting.value
						} 
						if(db){
							// let q = 'FOR d IN multi_channel_activity FILTER CONTAINS(LEFT(d.mid, 4), "'+ validationprc.principalId
							// +'") && d.issAcountId == "'+ validationprc.cardNumber
							// +'" && d.transDtTm > "'+ firstrule.period_start
							// +'" SORT d.transDtTm ASC LIMIT 1 RETURN d'
							let q = 'FOR d IN multi_channel_activity FILTER d.issAcountId == "'+ validationprc.cardNumber
							+'" SORT d.transDtTm ASC LIMIT 1 RETURN d'
							db.useDatabase(Env.get('ARANGO_DATABASE'))
							db.useBasicAuth(Env.get('ARANGO_USER'), Env.get('ARANGO_PASSWORD'))
							const query = await db.query(q)
							const data = await query.all()
							if(data){
								result.success = true
								result.data = data
							} else {
								result.success = true
								result.data = []
							}
						} else {
							result.connect = false
							result.message = dberr
						}
					}
				} 
			} catch(e) {
				result.message = e.message
			}
		}
		return result
	}
}

module.exports = new ArangoService