'use strict'

const Loyalty = use('App/Models/Loyalty')
const LoyaltyDetails = use('App/Models/LoyaltyDetails')
const Principal = use('App/Models/Principal')
const Merchant = use('App/Models/Merchant')
const Setting = use('App/Models/Setting')
const Reward = use('App/Models/Reward')
const BestSeller = use('App/Models/BestSeller')
const OurPick = use('App/Models/OurPick')
const Banner = use('App/Models/Banner')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const TadaService = require('./Helper/TadaService.js')
const MasterService = require('./Helper/MasterService.js')
const _lo = require('lodash')
const moment = require('moment')

class LoyaltySettingController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('loyaltysetting.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		// let tableDefinition = {
		// 	sTableName: 'loyalties',
		// 	sFromSql: "loyalties inner join loyalty_details on loyalty_details.loyalty_id = loyalties.id",
		// 	sSelectSql: ['loyalties.id', 'loyalties.status', 'principal_name', 'loyalty_details.name'],
		// 	aSearchColumns: ['principal_name', 'loyalty_details.name'],
		// 	dbType: 'postgres',
		// }

		let tableDefinition = {
			sTableName: 'loyalties',
			sSelectSql: ['loyalties.id', 'loyalties.status', 'principal_name'],
			aSearchColumns: ['principal_name'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)

		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)
		
		let selectRow = select.rows

		// let result = select.rows
		
		// var selectRow = _lo.chain(result).groupBy("id").map(function(v, i) {
		// 	return {
		// 		id: _lo.get(_lo.find(v, 'id'), 'id'),
		// 		status:  _lo.get(_lo.find(v, 'status'), 'status'),
		// 		principal_name:  _lo.get(_lo.find(v, 'principal_name'), 'principal_name'),
		// 	}
		// }).value();

		let fdata = []
		let no = 1
		for(let x in selectRow) {
			let loyaltyDetail = await LoyaltyDetails.query().select('id', 'name').where('loyalty_id', selectRow[x]['id']).fetch()
			let loyaltyDetailJson = loyaltyDetail.toJSON()
			let arr = []

			for(let y in loyaltyDetailJson) {
				arr.push(loyaltyDetailJson[y]['name'])
			}

			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['principal_name'],
				arr.length > 0 ? arr.join(", ") : '-',
				selectRow[x]['status'] == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Non Active</span>",
				"<div class='text-center'>\
					<a href='./loyaltysetting/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}

		let principals = await MasterService.getPrincipals()
		let principal = principals['data']['data']
		let dataPrincipal = []

		for(let i in principal) {
			dataPrincipal.push({
				id: principal[i]['mid'],
				text: principal[i]['name']
			})
		}
		
		let template = view.render('loyaltysetting.create', {
			meta: meta,
			principals: dataPrincipal
		})
		
		return await minifyHTML.minify(template)
	}

	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { principal_id, status, selected_rewards } = request.only(['principal_id', 'status', 'selected_rewards'])

		let principalSplit = principal_id.split("-")

		let formDataLoyalty = {
			principal_id: principalSplit[0],
			merchant_id: null,
			principal_name: principalSplit[1],
			merchant_name: null,
			status: parseInt(status),
			created_by: auth.user.id,
			updated_by: auth.user.id
		}

		const rules = {
			principal_id: 'required',
		}

		const validation = await validateAll(formDataLoyalty, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '400',
				message: 'Error Validation',
				data: [],
			}

			return response.send(data);
		} else {
			let checkPrincipal = await Loyalty.query().select('principal_id', 'principal_name').where('principal_id', principalSplit[0]).first()

			if(checkPrincipal) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '401',
					message: 'Loyalty Setting with Principal '+checkPrincipal.principal_name+', already exist',
					data: [],
				}

				return response.send(data);
			} else {
				let createLoyalty = await Loyalty.create(formDataLoyalty)

				let rewardJson = JSON.parse(selected_rewards)
				
				for(let x in rewardJson) {
					let formDataLoyaltyDetail = {
						loyalty_id: createLoyalty.id,
						name: rewardJson[x]['itemName'],
						category_id: rewardJson[x]['categoryId'],
						category_name: rewardJson[x]['categoryName'],
						brand: rewardJson[x]['brandName'],
						detail: rewardJson[x]['itemDescription'],
						expiry_date: rewardJson[x]['expDate'] != '-' ? moment(rewardJson[x]['expDate']).format('YYYY-MM-DD') : null,
						price: parseInt(rewardJson[x]['price']),
						selling_price: parseInt(rewardJson[x]['sellingPrice']),
						quota: rewardJson[x]['quota'] == null ? null : parseInt(rewardJson[x]['quota']),
						term_condition: rewardJson[x]['terms'] == '-' ? null : rewardJson[x]['terms'],
						location: null,
						image: rewardJson[x]['image'],
						tada_reward_id: parseInt(rewardJson[x]['itemId']),
						response: rewardJson[x]['response'],
						created_by: auth.user.id,
						updated_by: auth.user.id
					}

					await LoyaltyDetails.create(formDataLoyaltyDetail)
				}

				session.flash({ notification: 'Loyalty setting added', status: 'success' })
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '200',
					message: 'Loyalty setting added',
					data: [],
				}

				return response.send(data);
			}
		}
	}

	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let loyalty = await Loyalty.find(params.id)
		
		if (loyalty) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

			// Category
			const paramsCategory = {
				page: '1',
				perPage: '100'
			}

			let categories = await TadaService.getCategories(paramsCategory)
			let category = categories['data']
			let dataCategory = []

			for(let i in category) {
				dataCategory.push({
					id: category[i]['id'],
					text: category[i]['name']
				})
			}
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('loyaltysetting.edit', {
				meta: meta,
				loyalties: loyalty,
				categories: dataCategory
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Loyalty not found', status: 'danger' })
			return response.redirect('/loyaltysetting')
		}
	}

	async updateLoyalty({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { status, id } = request.only(['status', 'id'])
		
		let loyalty = await Loyalty.find(id)

		let formDataLoyalty = {
			status: parseInt(status),
			updated_by: auth.user.id
		}
		
		let rules = {
			status: 'required',
		}
		
		const validation = await validateAll(formDataLoyalty, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '400',
				message: 'Error Validation',
				data: [],
			}

			return response.send(data);
		} else {
			if (loyalty) {
				await Loyalty.query().where('id', id).update(formDataLoyalty)

				session.flash({ notification: 'Loyalty setting updated', status: 'success' })
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '200',
					message: 'Loyalty Setting updated',
					data: [],
				}

				return response.send(data);
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '401',
					message: 'Loyalty Setting not found',
					data: [],
				}

				return response.send(data);
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let loyalty = await Loyalty.find(formData.id)
		if (loyalty){
			try {
				await loyalty.delete()

				let loyaltyDetail = await LoyaltyDetails.query().where('loyalty_id', formData.id).fetch()
				let loyaltyDetailJson = loyaltyDetail.toJSON()

				for(let x in loyaltyDetailJson) {
					let loyaltyDetailDelete = await LoyaltyDetails.query().where('id', loyaltyDetailJson[x]['id']).first()

					try {
						await loyaltyDetailDelete.delete()
					} catch (e) {}

					let reward = await Reward.query().where('loyalty_detail_id', loyaltyDetailJson[x]['id']).fetch()
					let rewardJson = reward.toJSON()

					for(let y in rewardJson) {
						let rewardDelete = await Reward.query().where('id', rewardJson[y]['id']).first()

						try {
							await rewardDelete.delete()
						} catch (e) {}

						let ourpick = await OurPick.query().where('reward_id', rewardJson[y]['id']).fetch()
						let ourpickJson = ourpick.toJSON()

						for(let z in ourpickJson) {
							let ourpickDelete = await OurPick.query().where('id', ourpickJson[z]['id']).first()

							try {
								await ourpickDelete.delete()
							} catch (e) {}
						}

						let bestseller = await BestSeller.query().where('reward_id', rewardJson[y]['id']).fetch()
						let bestsellerJson = bestseller.toJSON()

						for(let z in bestsellerJson) {
							let bestsellerDelete = await BestSeller.query().where('id', bestsellerJson[z]['id']).first()

							try {
								await bestsellerDelete.delete()
							} catch (e) {}
						}

						let banner = await Banner.query().where('reward_id', rewardJson[y]['id']).fetch()
						let bannerJson = banner.toJSON()

						for(let z in bannerJson) {
							let bannerDelete = await Banner.query().where('id', bannerJson[z]['id']).first()

							try {
								await bannerDelete.delete()
							} catch (e) {}
						}
					}
				}

				session.flash({ notification: 'Loyalty Setting success deleted', status: 'success' })
				return response.redirect('/loyaltysetting')
			} catch (e) {
				session.flash({ notification: 'Loyalty Setting cannot be delete', status: 'danger' })
				return response.redirect('/loyaltysetting')
			}
		} else {
			session.flash({ notification: 'Loyalty Setting cannot be delete', status: 'danger' })
			return response.redirect('/loyaltysetting')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('LoyaltySetting', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let loyalty = await Loyalty.find(formData.item[i])
				try {
					await loyalty.delete()
				} catch (e) {}

				let loyaltyDetail = await LoyaltyDetails.query().where('loyalty_id', formData.item[i]).fetch()
				let loyaltyDetailJson = loyaltyDetail.toJSON()

				for(let x in loyaltyDetailJson) {
					let loyaltyDetailDelete = await LoyaltyDetails.query().where('id', loyaltyDetailJson[x]['id']).first()

					try {
						await loyaltyDetailDelete.delete()
					} catch (e) {}

					let reward = await Reward.query().where('loyalty_detail_id', loyaltyDetailJson[x]['id']).fetch()
					let rewardJson = reward.toJSON()

					for(let y in rewardJson) {
						let rewardDelete = await Reward.query().where('id', rewardJson[y]['id']).first()

						try {
							await rewardDelete.delete()
						} catch (e) {}

						let ourpick = await OurPick.query().where('reward_id', rewardJson[y]['id']).fetch()
						let ourpickJson = ourpick.toJSON()

						for(let z in ourpickJson) {
							let ourpickDelete = await OurPick.query().where('id', ourpickJson[z]['id']).first()

							try {
								await ourpickDelete.delete()
							} catch (e) {}
						}

						let bestseller = await BestSeller.query().where('reward_id', rewardJson[y]['id']).fetch()
						let bestsellerJson = bestseller.toJSON()

						for(let z in bestsellerJson) {
							let bestsellerDelete = await BestSeller.query().where('id', bestsellerJson[z]['id']).first()

							try {
								await bestsellerDelete.delete()
							} catch (e) {}
						}

						let banner = await Banner.query().where('reward_id', rewardJson[y]['id']).fetch()
						let bannerJson = banner.toJSON()

						for(let z in bannerJson) {
							let bannerDelete = await Banner.query().where('id', bannerJson[z]['id']).first()

							try {
								await bannerDelete.delete()
							} catch (e) {}
						}
					}
				}
			}
			session.flash({ notification: 'Loyalty Setting success deleted', status: 'success' })
			return response.redirect('/loyaltysetting')
		} else {
			session.flash({ notification: 'Loyalty Setting cannot be deleted', status: 'danger' })
			return response.redirect('/loyaltysetting')
		}
	}

	async deleteLoyaltyDetail({ request, response, auth, view, session }) {
		let req = request.all()

		let loyaltyDetail = await LoyaltyDetails.find(req.id)

		if(loyaltyDetail) {
			
			try {
				await loyaltyDetail.delete()
			} catch (e) {}

			let reward = await Reward.query().where('loyalty_detail_id', loyaltyDetail.id).fetch()
			let rewardJson = reward.toJSON()

			for(let y in rewardJson) {
				let rewardDelete = await Reward.query().where('id', rewardJson[y]['id']).first()

				try {
					await rewardDelete.delete()
				} catch (e) {}

				let ourpick = await OurPick.query().where('reward_id', rewardJson[y]['id']).fetch()
				let ourpickJson = ourpick.toJSON()

				for(let z in ourpickJson) {
					let ourpickDelete = await OurPick.query().where('id', ourpickJson[z]['id']).first()

					try {
						await ourpickDelete.delete()
					} catch (e) {}
				}

				let bestseller = await BestSeller.query().where('reward_id', rewardJson[y]['id']).fetch()
				let bestsellerJson = bestseller.toJSON()

				for(let z in bestsellerJson) {
					let bestsellerDelete = await BestSeller.query().where('id', bestsellerJson[z]['id']).first()

					try {
						await bestsellerDelete.delete()
					} catch (e) {}
				}

				let banner = await Banner.query().where('reward_id', rewardJson[y]['id']).fetch()
				let bannerJson = banner.toJSON()

				for(let z in bannerJson) {
					let bannerDelete = await Banner.query().where('id', bannerJson[z]['id']).first()

					try {
						await bannerDelete.delete()
					} catch (e) {}
				}
			}

			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '200',
				message: 'Loyalty detail success deleted',
				data: []
			}

			return response.send(data);
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '400',
				message: 'Loyalty detail not found',
				data: []
			}

			return response.send(data);
		}
	}

	async addLoyaltyDetail({ request, response, auth, view, session }) {
		let req = request.all()

		let dataJson = JSON.parse(req.data)

		let formDataLoyaltyDetail = {
			loyalty_id: req.id,
			name: dataJson.itemName,
			category_id: dataJson.categoryId,
			category_name: dataJson.categoryName,
			brand: dataJson.brandName,
			detail: dataJson.itemDescription,
			expiry_date: dataJson.expDate != '-' ? moment(dataJson.expDate).format('YYYY-MM-DD') : null,
			price: parseInt(dataJson.price),
			selling_price: parseInt(dataJson.sellingPrice),
			quota: dataJson.quota == null ? null : parseInt(dataJson.quota),
			term_condition: dataJson.terms == '-' ? null : dataJson.terms,
			location: null,
			image: dataJson.image,
			response: dataJson.response,
			tada_reward_id: parseInt(dataJson.itemId),
			created_by: auth.user.id,
			updated_by: auth.user.id
		}

		await LoyaltyDetails.create(formDataLoyaltyDetail)

		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '200',
			message: 'Loyalty detail added',
			data: [],
		}

		return response.send(data);
	}

	async editLoyaltyDetail({ request, response, auth, view, session }) {
		let req = request.all()

		let dataJson = JSON.parse(req.data)

		let formDataLoyaltyDetail = {
			name: dataJson.itemName,
			category_name: dataJson.categoryName,
			detail: dataJson.itemDescription,
			term_condition: dataJson.terms == '-' ? null : dataJson.terms,
			selling_price: parseInt(dataJson.sellingPrice),
			quota: dataJson.quota == null ? null : parseInt(dataJson.quota),
			updated_by: auth.user.id
		}

		await LoyaltyDetails.query().where('id', dataJson.id).update(formDataLoyaltyDetail)

		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '200',
			message: 'Loyalty detail updated',
			data: [],
		}

		return response.send(data);
	}

	async getLoyaltyDetail({ request, response, auth, view, session }) {
		let req = request.all()

		let data = await LoyaltyDetails.query().select('loyalty_details.*').where('loyalty_id', req.id).orderBy('id', 'asc').fetch()
		let dataJson = data.toJSON()
		let loyaltyDetail = []

		for(let x in dataJson) {
			loyaltyDetail.push({
				id: dataJson[x]['id'],
				itemId: dataJson[x]['tada_reward_id'],
				itemName: dataJson[x]['name'],
				itemDescription: dataJson[x]['detail'],
				categoryName: dataJson[x]['category_name'],
				brandName: dataJson[x]['brand'],
				expType: dataJson[x][''],
				exp: dataJson[x]['expiry_date'],
				expDate: dataJson[x]['expiry_date'],
				price: dataJson[x]['price'],
				sellingPrice: dataJson[x]['selling_price'],
				image: dataJson[x]['image'],
				terms: dataJson[x]['term_condition'],
				quota: dataJson[x]['quota'],
				response: dataJson[x]['response'],
			})
		}

		if(data) {
			data = {
				code: '2000',
				message: 'Loyalty detail Found',
				data: loyaltyDetail
			}
		} else {
			data = {
				code: '4000',
				message: 'Loyalty detail Not Found',
				data: []
			}
		}
		return response.send(data)
	}

	async getPrincipal({ request, response, auth, view, session }) {

		let principals = await MasterService.getPrincipals()
		let principal = principals['data']['data']
		let data = []

		for(let i in principal) {
			data.push({
				id: principal[i]['mid'],
				text: principal[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async getMerchant({ request, response, auth, view, session }) {
		let req = request.all()

		const params = {
			mid: req.id,
		}

		let merchants = await MasterService.getMerchants(params)
		let merchant = merchants['data']['data']
		let data = []

		for(let i in merchant) {
			data.push({
				id: merchant[i]['mid'],
				text: merchant[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async getTadaCategory({request, response, auth, view, session}) {
		const params = {
			page: '1',
			perPage: '100'
		}

		let categories = await TadaService.getCategories(params)
		let category = categories['data']
		let data = []

		for(let i in category) {
			data.push({
				id: category[i]['id'],
				text: category[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async getTadaReward({ request, response, view, session }) {
		let user = session.get('user')
		let req = request.all()

		const params = {
			categoryId: req.categoryId,
			page: req.page,
			perPage: '100'
		}

		let items = await TadaService.getItems(params)

		let item = items['data']['data']

		let data = []
		for(let i in item) {
			if(item[i].Item.active == true) {
				let variants = item[i].Item.Variants
				for(let j in variants) {
					if(variants[j].Stock.defaultExpDate) {
						let nowDate = moment(new Date()).format('YYYY-MM-DD')
						let fNowDate = moment(nowDate).valueOf()

						let expiry = moment(variants[j].Stock.defaultExpDate).format('YYYY-MM-DD')
						let fexpiry = moment(expiry).valueOf()

						if(fexpiry >= fNowDate) {
							data.push({
								itemId: item[i].Item.id,
								itemName: item[i].Item.name,
								itemDescription: item[i].Item.description ? item[i].Item.description : '-',
								categoryName: item[i].Item.Category.name,
								brandName: item[i].Item.Merchant ? item[i].Item.Merchant.brand : '-',
								expType: variants[j].Stock ? variants[j].Stock.defaultExpType : '-',
								exp: variants[j].Stock ? variants[j].Stock.defaultExp : '-',
								expDate: variants[j].Stock ? variants[j].Stock.defaultExpDate ? moment(variants[j].Stock.defaultExpDate).format('YYYY-MM-DD') : '-' : '-',
								price: variants[j].price,
								image: variants[j].image,
								terms: variants[j].Stock ? variants[j].Stock.terms : '-',
								response: JSON.stringify(item[i])
							})
						}
					} else {
						data.push({
							itemId: item[i].Item.id,
							itemName: item[i].Item.name,
							itemDescription: item[i].Item.description ? item[i].Item.description : '-',
							categoryName: item[i].Item.Category.name,
							brandName: item[i].Item.Merchant ? item[i].Item.Merchant.brand : '-',
							expType: variants[j].Stock ? variants[j].Stock.defaultExpType : '-',
							exp: variants[j].Stock ? variants[j].Stock.defaultExp : '-',
							expDate: variants[j].Stock ? variants[j].Stock.defaultExpDate ? moment(variants[j].Stock.defaultExpDate).format('YYYY-MM-DD') : '-' : '-',
							price: variants[j].price,
							image: variants[j].image,
							terms: variants[j].Stock ? variants[j].Stock.terms : '-',
							response: JSON.stringify(item[i])
						})
					}
				}
			}
		}

		if(items['success'] == true) {
			data = {
				code: '2000',
				message: 'Reward Found',
				data: data
			}
		} else {
			data = {
				code: '4000',
				message: 'Reward Not Found',
				data: []
			}
		}
		return response.send(data)
	}

	async getLoyaltyData({ request, response, auth, view, session }) {

		let req = request.get()
		let loyalties
		if (req.phrase != 'undefined') {
			loyalties = await Loyalty.query().where('category_name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			loyalties = await Loyalty.query().limit(20).fetch()
		}
		let loyalty = loyalties.toJSON()
		
		let data = []
		for(let i in loyalty){
			data.push({
				id: loyalty[i]['id'],
				text: loyalty[i]['category_name']
			})
		}
		
		return JSON.stringify({ results: data })
	
	}
}

module.exports = LoyaltySettingController
