'use strict'

const Principal = use('App/Models/Principal')
const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class PrincipalController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Principal', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('principal.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'principals',
			sFromSql: "principals inner join users on principals.user_id = users.id",
			sSelectSql: ['principals.id', 'users.fullname', 'principals.name', 'principals.principal_code'],
			aSearchColumns: ['users.fullname', 'principals.name', 'principals.principal_code'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 0
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				selectRow[x]['id'],
				selectRow[x]['fullname'],
				selectRow[x]['name'],
				selectRow[x]['principal_code'],
				"<div class='text-center'>\
					<a href='./principal/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('principal.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { user_id, name, principal_code } = request.only(['user_id', 'name', 'principal_code'])
		
		let formData = {
			user_id: user_id,
			name: name,
			principal_code: principal_code,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			user_id: 'required',
			name: 'required',
			principal_code: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Principal.create(formData)
			session.flash({ notification: 'Principal added', status: 'success' })
			return response.redirect('/principal')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let principal = await Principal.find(params.id)
		let user = await User.query().select('fullname','email').where('id', principal.user_id).first()

		if (principal) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('principal.edit', {
				meta: meta,
				principal: principal,
				user: user.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Principal not found', status: 'danger' })
			return response.redirect('/principal')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { user_id, name, principal_code } = request.only(['user_id', 'name', 'principal_code'])
		
		let principal = await Principal.find(params.id)

		let formData = {
			user_id: user_id,
			name: name,
			principal_code: principal_code,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		let rules = {
			user_id: 'required',
			name: 'required',
			principal_code: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (principal) {
				await Principal.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Principal updated', status: 'success' })
				return response.redirect('/principal')
			} else {
				session.flash({ notification: 'Principal not found', status: 'danger' })
				return response.redirect('/principal')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let principal = await Principal.find(formData.id)
		if (principal){
			try {
				await principal.delete()
				session.flash({ notification: 'Principal success deleted', status: 'success' })
				return response.redirect('/principal')
			} catch (e) {
				session.flash({ notification: 'Principal cannot be delete', status: 'danger' })
				return response.redirect('/principal')
			}
		} else {
			session.flash({ notification: 'Principal cannot be delete', status: 'danger' })
			return response.redirect('/principal')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Principal', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let principal = await Principal.find(formData.item[i])
				try {
					await principal.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Principal success deleted', status: 'success' })
			return response.redirect('/principal')
		} else {
			session.flash({ notification: 'Principal cannot be deleted', status: 'danger' })
			return response.redirect('/principal')
		}
	}

	async getPrincipal({request, response, auth, view, session}) {
		let req = request.get()
		let principals
		if (req.phrase != 'undefined') {
			principals = await Principal.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			principals = await Principal.query().limit(20).fetch()
		}
		let principal = principals.toJSON()
		
		let data = []
		for(let i in principal){
			data.push({
				id: principal[i]['id'],
				text: principal[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = PrincipalController
