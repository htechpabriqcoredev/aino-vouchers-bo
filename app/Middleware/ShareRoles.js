'use strict'

const Logger = use('Logger')

class ShareRoles {
	async handle({ request, response, session, auth }, next) {
		let checkAuth = await this.checkAuth(auth)
		if (checkAuth) {
			let checkUser = await this.checkUser(auth)
			if (checkUser) {
				let user = auth.user
				let get_roles = await user.roles().fetch()
				let users_roles_json = get_roles.toJSON()
				let users_roles = []
				for(let i in users_roles_json){
					users_roles.push(users_roles_json[i].role_slug)
				}
				session.flash({ users_roles: users_roles })
				
				await next()
			} else {
				return response.redirect('/login')
			}
		} else {
			return response.redirect('/login')
		}
	}
	
	async checkAuth(auth) {
		try {
			return await auth.check()
		} catch(error) {
			return false
		}
	}
	
	async checkUser(auth) {
		try {
			return await auth.getUser()
		} catch(error) {
			return false
		}
	}
}

module.exports = ShareRoles