# AINO Backoffice 1.0.0

This is engine for your project

Installation:
1. First, download nodejs package from https://nodejs.org/en/download/
2. Once the installation is complete, install the nodejs package
3. The cli tool is installed globally using npm npm i -g @adonisjs/cli.
4. Once the installation completes, make sure that you can run adonis from your command line adonis --help.
5. Copy folder �aino project�.
6. Install all package from npm npm install.
7. Create database using adonis migration:run.
8. Once the installation process is completed, you can cd into the directory and run the following command to start the HTTP server adonis serve --dev. info: serving app on 127.0.0.1:3333.

Server Spec (Minimum Recomendation)
- CORE CPUs : 2 Cores
- Memory : 2 GB
- HDD : 100 GB