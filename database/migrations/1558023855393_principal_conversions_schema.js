'use strict'

const Schema = use('Schema')

class PrincipalConversionsSchema extends Schema {
	up () {
		this.create('principal_conversions', (table) => {
			table.increments()
			table.integer('user_id')
			table.string('principal_id')
			table.string('principal_code')
			table.integer('amount')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('principal_conversions')
	}
}

module.exports = PrincipalConversionsSchema
