'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('email', 255).notNullable().unique()
      table.string('password', 60).notNullable()
	  table.string('fullname', 255).nullable()
	  table.integer('user_type', 11).defaultTo(1)
	  table.integer('user_role', 11).defaultTo(1)
	  table.text('social_token').nullable()
	  table.string('login_source', 20).defaultTo('web')
	  table.enu('block', ['Y', 'N']).defaultTo('N')
	  table.string('activation_key', 60).nullable()
	  table.string('forget_key', 60).nullable()
	  table.string('player_id', 255).nullable()
	  table.string('player_id_mobile', 255).nullable()
	  table.string('telephone', 20).nullable()
	  table.string('telephone_otp', 10).nullable()
	  table.integer('telephone_verified', 1).defaultTo(0)
	  table.integer('status', 1).defaultTo(0)
	  table.string('principal_id').nullable()
	  table.string('principal_code').nullable()
	  table.integer('created_by', 10).defaultTo(1)
	  table.integer('updated_by', 10).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
