'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LoyaltyDetailsSchema extends Schema {
  up () {
    this.create('loyalty_details', (table) => {
    	table.increments()
		table.integer('loyalty_id')
		table.string('name')
		table.string('category_name')
		table.string('brand')
		table.text('detail')
		table.date('expiry_date')
		table.integer('price')
		table.integer('selling_price')
		table.integer('quota')
		table.text('term_condition')
		table.text('location')
		table.string('image')
		table.string('tada_reward_id')
		table.string('category_id')
		table.text('response')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('loyalty_details')
  }
}

module.exports = LoyaltyDetailsSchema
