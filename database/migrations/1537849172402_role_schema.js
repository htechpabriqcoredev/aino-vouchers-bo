'use strict'

const Schema = use('Schema')

class RoleSchema extends Schema {
	up () {
		this.create('roles', (table) => {
			table.increments()
			table.string('role_title', 255)
			table.string('role_slug', 255)
			table.text('role_access', 'longtext')
			table.integer('created_by', 10).defaultTo(1)
			table.integer('updated_by', 10).defaultTo(1)
			table.timestamps()
		})
	}

	down () {
		this.drop('roles')
	}
}

module.exports = RoleSchema
