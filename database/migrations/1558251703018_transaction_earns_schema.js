'use strict'

const Schema = use('Schema')

class TransactionEarnsSchema extends Schema {
	up () {
		this.create('transaction_earns', (table) => {
			table.increments()
			table.integer('user_id')
			table.string('user_name')
			table.integer('issuer_id')
			table.string('issuer_name')
			table.integer('card_id')
			table.string('card_number')
			table.integer('reward_id')
			table.string('activity_type')
			table.string('spending_type')
			table.integer('amount')
			table.integer('reward_amount')
			table.string('reward_type')
			table.datetime('date')
			table.string('location')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('transaction_earns')
	}
}

module.exports = TransactionEarnsSchema
