'use strict'

const Schema = use('Schema')

class BannersSchema extends Schema {
	up () {
		this.create('banners', (table) => {
	  		table.increments()
			table.integer('user_id')
			table.string('principal_id')
			table.string('principal_code')
			table.string('title')
			table.date('start_date')
			table.time('start_hour')
			table.date('end_date')
			table.time('end_hour')
			table.string('picture')
			table.string('link')
			table.integer('reward_id')
			table.integer('status')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('banners')
	}
}

module.exports = BannersSchema
