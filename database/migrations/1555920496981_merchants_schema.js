'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MerchantsSchema extends Schema {
  up () {
    this.create('merchants', (table) => {
    	table.increments()
		table.integer('principal_id')
		table.string('name')
		table.string('merchant_code')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('merchants')
  }
}

module.exports = MerchantsSchema
