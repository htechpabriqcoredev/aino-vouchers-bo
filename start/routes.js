'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Route = use('Route')

// Router for Frontend
Route.get('/', 'AccountController.getIndex')
Route.get('login', 'AccountController.getLogin')
Route.post('login', 'AccountController.postLogin')
Route.get('logout', 'AccountController.getLogout')
Route.get('error', 'ErrorController.index')

// Router for Backend
Route.group(() => {
	Route.get('home', 'HomeController.getDashboard')
	
	Route.get('filemanager', 'FilemanagerController.index')
	Route.get('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/upload', 'FilemanagerController.upload').formats(['json'])
	Route.post('filemanager/delete', 'FilemanagerController.delete').formats(['json'])

	Route.post('transaction-earn/datatable', 'TransactionEarnController.datatable')
	Route.resource('transaction-earn', 'TransactionEarnController')

	Route.post('transaction-burn/datatable', 'TransactionBurnController.datatable')
	Route.resource('transaction-burn', 'TransactionBurnController')

	Route.post('rule/checkexist', 'RuleController.checkexist')
	Route.post('rule/checkexistedit', 'RuleController.checkexistedit')
	Route.get('rule/getcard', 'RuleController.getCard').formats(['json'])
	Route.post('rule/datatable', 'RuleController.datatable')
	Route.post('rule/delete', 'RuleController.delete')
	Route.post('rule/multidelete', 'RuleController.multidelete')
	Route.resource('rule', 'RuleController')

	Route.get('customer/getcustomer', 'CustomerController.getCustomer').formats(['json'])
	Route.get('customer/:id/updatestatus', 'CustomerController.updateStatus')
	Route.post('customer/datatable', 'CustomerController.datatable')
	Route.post('customer/delete', 'CustomerController.delete')
	Route.post('customer/multidelete', 'CustomerController.multidelete')
	Route.resource('customer', 'CustomerController')

	Route.get('card/getcard', 'CardController.getCard').formats(['json'])
	Route.post('card/datatable', 'CardController.datatable')
	Route.post('card/delete', 'CardController.delete')
	Route.post('card/multidelete', 'CardController.multidelete')
	Route.resource('card', 'CardController')

	Route.get('issuer/getissuer', 'IssuerController.getIssuer').formats(['json'])
	Route.post('issuer/datatable', 'IssuerController.datatable')
	Route.post('issuer/delete', 'IssuerController.delete')
	Route.post('issuer/multidelete', 'IssuerController.multidelete')
	Route.resource('issuer', 'IssuerController')

	Route.get('merchant/getmerchant', 'MerchantController.getMerchant').formats(['json'])
	Route.post('merchant/datatable', 'MerchantController.datatable')
	Route.post('merchant/delete', 'MerchantController.delete')
	Route.post('merchant/multidelete', 'MerchantController.multidelete')
	Route.resource('merchant', 'MerchantController')

	Route.get('principal/getprincipal', 'PrincipalController.getPrincipal').formats(['json'])
	Route.post('principal/datatable', 'PrincipalController.datatable')
	Route.post('principal/delete', 'PrincipalController.delete')
	Route.post('principal/multidelete', 'PrincipalController.multidelete')
	Route.resource('principal', 'PrincipalController')
	
	Route.get('users/getuser', 'UserController.getUser').formats(['json'])
	Route.post('users/datatable', 'UserController.datatable')
	Route.post('users/delete', 'UserController.delete')
	Route.post('users/multidelete', 'UserController.multidelete')
	Route.resource('users', 'UserController')
	
	Route.post('role/datatable', 'RoleController.datatable')
	Route.post('role/delete', 'RoleController.delete')
	Route.post('role/multidelete', 'RoleController.multidelete')
	Route.resource('role', 'RoleController')
	
	Route.post('roles-users/datatable', 'RolesUsersController.datatable')
	Route.post('roles-users/delete', 'RolesUsersController.delete')
	Route.post('roles-users/multidelete', 'RolesUsersController.multidelete')
	Route.resource('roles-users', 'RolesUsersController')
	
	Route.post('setting/updateall', 'SettingController.updateall')
	Route.resource('setting', 'SettingController')
	
	Route.post('apiservice/datatable', 'ApiServiceController.datatable')
	Route.post('apiservice/delete', 'ApiServiceController.delete')
	Route.post('apiservice/multidelete', 'ApiServiceController.multidelete')
	Route.resource('apiservice', 'ApiServiceController')
	
	Route.post('systemlog/datatable', 'SystemLogController.datatable')
	Route.post('systemlog/delete', 'SystemLogController.delete')
	Route.post('systemlog/multidelete', 'SystemLogController.multidelete')
	Route.resource('systemlog', 'SystemLogController')

	Route.post('loyaltysetting/updateloyalty', 'LoyaltySettingController.updateLoyalty')
	Route.post('loyaltysetting/addloyaltydetail', 'LoyaltySettingController.addLoyaltyDetail')
	Route.post('loyaltysetting/editloyaltydetail', 'LoyaltySettingController.editLoyaltyDetail')
	Route.post('loyaltysetting/deleteloyaltydetail', 'LoyaltySettingController.deleteLoyaltyDetail')
	Route.post('loyaltysetting/getloyaltydetail', 'LoyaltySettingController.getLoyaltyDetail').formats(['json'])
	Route.get('loyaltysetting/getprincipal', 'LoyaltySettingController.getPrincipal').formats(['json'])
	Route.post('loyaltysetting/getmerchant', 'LoyaltySettingController.getMerchant').formats(['json'])
	Route.post('loyaltysetting/gettadareward', 'LoyaltySettingController.getTadaReward').formats(['json'])
	Route.get('loyaltysetting/gettadacategory', 'LoyaltySettingController.getTadaCategory').formats(['json'])
	Route.post('loyaltysetting/datatable', 'LoyaltySettingController.datatable')
	Route.post('loyaltysetting/delete', 'LoyaltySettingController.delete')
	Route.post('loyaltysetting/multidelete', 'LoyaltySettingController.multidelete')
	Route.resource('loyaltysetting', 'LoyaltySettingController')

	Route.post('vouchermanagement/convertpoint', 'VoucherManagementController.convertPoint')
	Route.post('vouchermanagement/getvoucher', 'VoucherManagementController.getVoucher')
	Route.post('vouchermanagement/getdata', 'VoucherManagementController.getDetail')
	Route.get('vouchermanagement/getbrand', 'VoucherManagementController.getBrand')
	Route.post('vouchermanagement/datatable', 'VoucherManagementController.datatable')
	Route.post('vouchermanagement/delete', 'VoucherManagementController.delete')
	Route.post('vouchermanagement/multidelete', 'VoucherManagementController.multidelete')
	Route.get('vouchermanagement/getdatavoucher', 'VoucherManagementController.getDataVoucher')
	Route.resource('vouchermanagement', 'VoucherManagementController')

	Route.post('principal-conversion/update', 'PrincipalConversionController.update')
	Route.resource('principal-conversion', 'PrincipalConversionController')

	Route.post('banner/datatable', 'BannerController.datatable')
	Route.post('banner/delete', 'BannerController.delete')
	Route.post('banner/multidelete', 'BannerController.multidelete')
	Route.resource('banner', 'BannerController')

	Route.post('best-seller/datatable', 'BestSellerController.datatable')
	Route.post('best-seller/delete', 'BestSellerController.delete')
	Route.post('best-seller/multidelete', 'BestSellerController.multidelete')
	Route.post('best-seller/getdatareward', 'BestSellerController.getDataReward')
	Route.resource('best-seller', 'BestSellerController')

	Route.post('our-pick/datatable', 'OurPickController.datatable')
	Route.post('our-pick/delete', 'OurPickController.delete')
	Route.post('our-pick/multidelete', 'OurPickController.multidelete')
	Route.post('our-pick/getdatareward', 'OurPickController.getDataReward')
	Route.resource('our-pick', 'OurPickController')

	Route.post('theme/update', 'ThemeController.update')
	Route.resource('theme', 'ThemeController')
})
.middleware('auth')
.middleware('roles')
.middleware('globalparam')

// Router for API
require('./api.js')